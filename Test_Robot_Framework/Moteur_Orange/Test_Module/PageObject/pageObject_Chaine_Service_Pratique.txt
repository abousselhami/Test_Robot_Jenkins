*** Settings ***
Resource          ../Conf/conf_Chaine_Service_Pratique.txt

*** Keywords ***
!contenu
    [Arguments]    ${locator}
    Element Should Be Visible    ${locator}/div[2]
    Page Should Contain Link    ${locator}/div[2]/div[1]/a
    Click Element    ${locator}/div[2]/div[1]/a
    sleep    2s
    Select Window    NEW
    Wait Until Page Contains Element    o-megamenu-link    30
    Location Should Contain    orange
    Select Window    MAIN
    Element Should Be Visible    ${locator}/div[2]/div[2]
    Element Should Be Visible    ${locator}/div[2]/div[3]
    couleur    ${locator}/div[2]/div[3]    rgb(136, 136, 136)    gris
    ##############
    Element Should Be Visible    ${locator}/div[3]
    Page Should Contain Link    ${locator}/div[3]/div[1]/a
    Click Element    ${locator}/div[3]/div[1]/a
    sleep    2s
    Select Window    NEW
    Wait Until Page Contains Element    dzr-app    30
    Location Should Contain    orange
    Select Window    MAIN
    Element Should Be Visible    ${locator}/div[3]/div[2]
    Element Should Be Visible    ${locator}/div[3]/div[3]
    couleur    ${locator}/div[3]/div[3]    rgb(136, 136, 136)    gris
