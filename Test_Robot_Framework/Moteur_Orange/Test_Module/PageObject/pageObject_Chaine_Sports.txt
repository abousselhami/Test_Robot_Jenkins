*** Settings ***
Resource          ../Conf/conf_Chaine_Sports.txt

*** Keywords ***
!Description
    [Arguments]    ${locator}
    Element Should Be Visible    ${locator}

!verifier_chaine_block
    [Arguments]    ${locator}
    Element Should Be Visible    ${locator}
    Title    ${locator}/div[1]/a    sports.orange.fr
    couleur    ${locator}/div[1]    rgb(0, 68, 187)    bleu
    File_Ariane    ${locator}/div[2]    Orange Sports >
    Image    ${locator}/a    sports.orange.fr
    !Description    ${locator}/div[3]/div
