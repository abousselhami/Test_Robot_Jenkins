*** Settings ***
Resource          ../PageObject/pageObject_Abstract.txt

*** Keywords ***
Header
    [Arguments]    ${locator}    ${text}
    !Header    ${locator}    ${text}

Block
    [Arguments]    ${locator}
    !Block    ${locator}

Title
    [Arguments]    ${locator}    ${location}
    !Title    ${locator}    ${location}

ImageTitle
    [Arguments]    ${locator}
    !ImageTitle    ${locator}

Image
    [Arguments]    ${locator}    ${location}
    !Image    ${locator}    ${location}

getDayName
    ${day}=    !getDayName
    [Return]    ${day}

getMonthName
    ${month}=    !getMonthName
    [Return]    ${month}

siteLink
    [Arguments]    ${locator}    ${titre_sitelink}
    !siteLink    ${locator}    ${titre_sitelink}

File_Ariane
    [Arguments]    ${locator}    ${text}
    !File_Ariane    ${locator}    ${text}

footer
    [Arguments]    ${locator}    ${text}
    !footer    ${locator}    ${text}

footerLlink
    [Arguments]    ${locator}    ${location}
    !footerLink    ${locator}    ${location}

footerLink_Chaine
    [Arguments]    ${locator}    ${location}
    !footerLink_Chaine    ${locator}    ${location}

click_Element_New_Page
    [Arguments]    ${locator}    ${location}    ${element}
    !click_Element_New_Page    ${locator}    ${location}    ${element}

couleur
    [Arguments]    ${locator}    ${rgb}    ${col}
    !couleur    ${locator}    ${rgb}    ${col}

click_Element_Same_Page
    [Arguments]    ${locator}    ${location}    ${element}
    !click_Element_Same_Page    ${locator}    ${location}    ${element}
