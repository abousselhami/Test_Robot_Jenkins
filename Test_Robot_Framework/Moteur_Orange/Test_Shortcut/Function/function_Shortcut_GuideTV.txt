*** Settings ***
Resource          ../PageObject/pageObject_Shortcut_GuideTV.txt

*** Keywords ***
verifier_shortcut_guideTV
    [Arguments]    ${kw}    ${locator}
    Open Browser    https://lemoteur.orange.fr/?module=orange&bhv=web_fr&kw=${kw}&profil=orange2    FF
    Block    ${locator}
    Header    ${locator}/div[1]/div[1]    PROGRAMME TV
    !headerDate    ${locator}/div[1]/div[2]
    couleur    ${locator}/a    rgb(0, 68, 187)    bleu
    Title    ${locator}/a    programme-tv.orange.fr
    !contenu    ${locator}/div[2]
    footer    ${locator}/div[4]/div    programme-tv.orange.fr
