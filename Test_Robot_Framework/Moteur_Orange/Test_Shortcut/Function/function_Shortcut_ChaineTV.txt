*** Settings ***
Resource          ../PageObject/pageObject_Shortcut_ChaineTV.txt

*** Keywords ***
verifier_shortcut_chaineTV
    [Arguments]    ${kw}    ${locator}
    Open Browser    https://lemoteur.orange.fr/?module=orange&bhv=web_fr&kw=${kw}&profil=orange2    FF
    Block    ${locator}
    Header    ${locator}/div[1]    PROGRAMME TV
    !contenuTvChannel    ${locator}/div[2]
    footerLlink    ${locator}/div[3]    programme-tv.orange.fr
