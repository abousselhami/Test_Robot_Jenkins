*** Settings ***
Resource          ../PageObject/pageObject_Shortcut_Sport.txt

*** Keywords ***
verifier_shortcut_sport
    [Arguments]    ${kw}    ${locator}
    Open Browser    https://lemoteur.orange.fr/?module=orange&bhv=web_fr&kw=${kw}&profil=orange2    FF
    Block    ${locator}
    Header    ${locator}/div[1]    CERCLE SPORTIF
    couleur    ${locator}/div[2]/div[1]/a    rgb(0, 68, 187)    bleu
    Title    ${locator}/div[2]/div[1]/a    sports.orange.fr
    ImageTitle    ${locator}/div[2]/div[2]/a/img
    !description    ${locator}/div[2]/div[3]

verifier_shortcut_Top14
    [Arguments]    ${kw}    ${locator}
    Open Browser    https://lemoteur.orange.fr/?module=orange&bhv=web_fr&kw=${kw}&profil=orange2    FF
    Block    ${locator}
    Header    ${locator}/div[1]    TOP 14
    couleur    ${locator}/div[2]/div[1]/a    rgb(0, 68, 187)    bleu
    Title    ${locator}/div[2]/div[1]/a    sports.orange.fr
    ImageTitle    ${locator}/div[2]/div[2]/a/div
    !description_Top14    ${locator}/div[2]/div[3]
    footer    ${locator}/div[2]/div[4]    sports.orange.fr/rugby/top-14
