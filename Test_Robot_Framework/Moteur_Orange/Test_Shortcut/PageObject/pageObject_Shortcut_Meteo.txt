*** Settings ***
Resource          ../Conf/conf_Shortcut_Meteo.txt

*** Keywords ***
!description
    [Arguments]    ${locator}
    Element Should Be Visible    ${locator}
    Xpath Should Match X Times    ${locator}/li    4
    ${count}=    Get Matching Xpath Count    ${locator}/li
    : FOR    ${c}    IN RANGE    1    ${count}+1
    \    Element Should Be Visible    ${locator}/li[${c}]/a/div
    \    Click Element    ${locator}/li[${c}]/a/div
    \    sleep    2s
    \    Select Window    NEW
    \    Wait Until Page Contains Element    o-megamenu-link    30
    \    Location Should Contain    meteo.orange.fr
    \    Select Window    MAIN
    \    Element Should Be Visible    ${locator}/li[${c}]/div
    \    Run Keyword If    ${c} == '1'    Element Text Should Be    ${locator}/li[${c}]/div    en ce moment
    \    ...    ELSE IF    ${c} == '2'    Element Text Should Be    ${locator}/li[${c}]/div    Après-midi
    \    ...    ELSE IF    ${c} == '3'    Element Text Should Be    ${locator}/li[${c}]/div    Soir
    \    ...    ELSE IF    ${c} == '4'    Element Text Should Be    ${locator}/li[${c}]/div    Nuit
    \    Element Should Be Visible    ${locator}/li[${c}]/div/span[1]
    \    Element Should Be Visible    ${locator}/li[${c}]/div/span[2]
